package listeners;

import org.openqa.selenium.WebDriver;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;
import testUtilities.CaptureWebScreenshot;

public class ScreenshotListener implements ITestListener {
    WebDriver driver=null;
    @Override
    public void onTestStart(ITestResult iTestResult) {
        System.out.println("***** "+iTestResult.getName()+" test has started *****");
    }

    @Override
    public void onTestSuccess(ITestResult iTestResult) {
        System.out.println("*****"+iTestResult.getName()+" test has Passed *****");
        CaptureWebScreenshot ss=new CaptureWebScreenshot();
        ss.capture();
    }

    @Override
    public void onTestFailure(ITestResult iTestResult) {
        if(!iTestResult.isSuccess()){
            System.out.println("***** Error "+iTestResult.getName()+" test has failed *****");
            CaptureWebScreenshot ss=new CaptureWebScreenshot();
            ss.capture();
        }

    }

    @Override
    public void onTestSkipped(ITestResult iTestResult) {

    }

    @Override
    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    @Override
    public void onStart(ITestContext iTestContext) {
        System.out.println("***** "+iTestContext.getName()+" test has started *****");
    }

    @Override
    public void onFinish(ITestContext iTestContext) {
        System.out.println("***** "+iTestContext.getName()+" test has ended *****");
    }
}
