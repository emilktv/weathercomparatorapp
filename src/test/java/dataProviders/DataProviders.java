package dataProviders;

import org.testng.annotations.DataProvider;
import utilities.PropertyReader;

import java.io.IOException;
import java.util.Properties;

public class DataProviders {
    //DataProviders for valid and Invalid Inputs

    /*
     * DataProvider to test variance calculator within variance range
     * Boundary values of variance range taken as test input
     */
    @DataProvider(name="valuesWithinVarianceRange")
    public static Object[][] valuesWithinVarianceRange() throws IOException {
        Properties properties=new PropertyReader().readProperty(System.getProperty("env"));
        double varianceRange=Double.parseDouble(properties.getProperty("varianceRange"));
        return new Double[][]{
                {0.0,varianceRange},
                {0.0,varianceRange-1.0},
                {varianceRange,varianceRange},
                {varianceRange,0.0},
                {varianceRange-1.0,0.0}
        };
    }
    //Boundary values outside varianceRange
    @DataProvider(name="valuesOutsideVarianceRange")
    public static Object[][] valuesOutsideVarianceRange() throws IOException {
        Properties properties=new PropertyReader().readProperty(System.getProperty("env"));
        double varianceRange=Double.parseDouble(properties.getProperty("varianceRange"));
        return new Double[][]{
                {0.0,varianceRange+1.0},
                {0.0,varianceRange+2.0}
        };
    }


}
