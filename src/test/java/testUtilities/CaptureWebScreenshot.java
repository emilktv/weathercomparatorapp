package testUtilities;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import tests.unit.AccuWeatherUITest;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaptureWebScreenshot extends AccuWeatherUITest {
    public void capture(){
        try{
            TakesScreenshot scrShot =((TakesScreenshot)driver);
            File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
            String filename =  new SimpleDateFormat("yyyyMMddhhmmss'.jpeg'").format(new Date());
            File DestFile=new File("./src/test/TestScreenShots/"+filename);
            FileUtils.copyFile(SrcFile, DestFile);}
        catch (Exception e){
            System.out.println(e.toString());
        }
        System.out.println("Screenshot Captured");
    }
}
