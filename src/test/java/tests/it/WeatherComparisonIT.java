package tests.it;

import exceptions.MatcherException;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import utilities.DriverFactory;
import utilities.PropertyReader;
import weatherComparator.WeatherComparator;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class WeatherComparisonIT {
    WebDriver driver;
    Properties properties;
    @BeforeMethod
    public void setUp() throws IOException {
        properties=new PropertyReader().readProperty("qa");
        driver= DriverFactory.getDriver(properties.getProperty("browserName"));
        driver.get(properties.getProperty("url"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Test
    public void shouldBeAbleToGetComparison() throws Exception {
        WeatherComparator weatherComparator=new WeatherComparator();
        Properties properties=new PropertyReader().readProperty(System.getProperty("env"));
        try{
        String result=weatherComparator.compareMetric(properties.getProperty("metricName"),driver);
        Assert.assertEquals(result,"Metrics match");}
        catch(MatcherException m){
            Assert.assertEquals(m.getMessage(),"Metrics do not match");
        }
    }

    @AfterMethod
    public void tearDown(){
        DriverFactory.closeAllDriver();
    }
}
