package tests.unit;

import dataProviders.DataProviders;
import exceptions.MatcherException;
import org.testng.Assert;
import org.testng.annotations.Test;
import weatherComparator.MetricVarianceCalculator;

import java.io.IOException;

public class MetricVarianceCalculatorTest {
    @Test(dataProvider="valuesWithinVarianceRange",dataProviderClass= DataProviders.class)
    public void shouldBeAbleToGetMetricsMatch(Double metric1,Double metric2) throws MatcherException, IOException {
        MetricVarianceCalculator metricVarianceCalculator=new MetricVarianceCalculator();
        Assert.assertTrue(metricVarianceCalculator.getMetricVariance(metric1,metric2).equals("Metrics match"));
    }

    @Test(dataProvider = "valuesOutsideVarianceRange",dataProviderClass = DataProviders.class,expectedExceptions = {MatcherException.class},expectedExceptionsMessageRegExp = "Metrics do not match")
    public void shouldRaiseException(Double metric1,Double metric2) throws MatcherException, IOException {
        MetricVarianceCalculator metricVarianceCalculator=new MetricVarianceCalculator();
        metricVarianceCalculator.getMetricVariance(metric1,metric2);
    }
}
