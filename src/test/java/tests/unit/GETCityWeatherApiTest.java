package tests.unit;

import clients.GETCityWeatherClient;
import clients.response.GETCityWeatherResponse;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import utilities.PropertyReader;

import java.io.IOException;
import java.util.Properties;

public class GETCityWeatherApiTest {
    GETCityWeatherClient getCityWeatherClient;
    Properties properties;
    @BeforeTest
    public void instantiateObjects() throws IOException {
        getCityWeatherClient=new GETCityWeatherClient();
        properties=new PropertyReader().readProperty(System.getProperty("env"));
    }
    @Test
    public void shouldBeAbleToGetCityWeather() throws IOException {
        GETCityWeatherResponse getCityWeatherResponse =getCityWeatherClient.getCityWeather();
        System.out.println(getCityWeatherResponse.getName());
        System.out.println(properties.getProperty("cityName"));
        Assert.assertTrue(getCityWeatherResponse.getName().equals(properties.getProperty("cityName")));
    }
}
