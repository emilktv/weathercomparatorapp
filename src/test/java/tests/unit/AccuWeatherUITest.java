package tests.unit;

import facade.GetUIWeatherDetailsFacade;
import listeners.ScreenshotListener;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import utilities.DriverFactory;
import utilities.WeatherDetails;
import utilities.PropertyReader;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
@Listeners(listeners.ScreenshotListener.class)
public class AccuWeatherUITest {
    public static WebDriver driver;
    Properties properties;
    WeatherDetails weatherDetails;

    @BeforeMethod
    public void setup() throws IOException {
        properties=new PropertyReader().readProperty(System.getProperty("env"));
        driver=DriverFactory.getDriver(properties.getProperty("browserName"));
        driver.get(properties.getProperty("url"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }
    @Test
    public void shouldBeAbleToGetWeatherDetails() throws IOException {
        GetUIWeatherDetailsFacade facade = new GetUIWeatherDetailsFacade(driver);
        facade.getUiWeatherDetailsFacade(properties.getProperty("cityName"));
        Assert.assertTrue(facade.getCurrentWeatherDetailsPage().getCityNameHeaderText().contains(properties.getProperty("cityName")));
    }
    @AfterMethod
    public void tearDown(){
        DriverFactory.closeAllDriver();
    }

}
