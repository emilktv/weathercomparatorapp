package clients;

import clients.response.GETCityWeatherResponse;
import com.google.gson.Gson;
import io.restassured.response.Response;
import utilities.PropertyReader;


import java.io.IOException;
import java.util.Properties;

import static io.restassured.RestAssured.given;

//GETCityWeather Api Client
public class GETCityWeatherClient {

    public GETCityWeatherResponse getCityWeather() throws IOException {
        Properties properties=new PropertyReader().readProperty("qa");
        Response cityWeather=given()
                .queryParam("q",properties.getProperty("cityName"))
                .queryParam("units","metric")
                .queryParam("appid",properties.getProperty("apiKey"))
                .when()
                .get(properties.getProperty("apiBasepath")+properties.getProperty("apiEndpoint"));
        return new Gson().fromJson(cityWeather.body().asString(),GETCityWeatherResponse.class);
    }
}
