package weatherComparator;

//This Class is used to store results from accuWeather and API .The results are compared with the help of this class.
public class WeatherComparisonMetrics {
    double temperature;
    double pressure;
    double humidity;

    public double getTemperature() {
        return temperature;
    }

    public double getHumidity() {
        return humidity;
    }

    public double getPressure() {
        return pressure;
    }

    public void setWeatherMetrics(double temperature,double pressure,double humidity){
        this.temperature=temperature;
        this.pressure=pressure;
        this.humidity=humidity;
    }
}
