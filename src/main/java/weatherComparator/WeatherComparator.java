package weatherComparator;

import clients.GETCityWeatherClient;
import clients.response.GETCityWeatherResponse;
import facade.GetUIWeatherDetailsFacade;
import org.openqa.selenium.WebDriver;
import utilities.DriverFactory;
import utilities.PropertyReader;
import utilities.WeatherDetails;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

//Methods in this class compare the weather results from accuWeather and Api
public class WeatherComparator {
    Properties properties;
    public static WebDriver driver;
    //Setup method for WebDriver
    public WebDriver driverSetup() throws IOException {
        properties=new PropertyReader().readProperty(System.getProperty("env"));
        driver= DriverFactory.getDriver(properties.getProperty("browserName"));
        driver.get(properties.getProperty("url"));
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        return driver;
    }
    //Method to get weather details from accuWeather
    public WeatherComparisonMetrics getAccuWeatherMetrics(WebDriver driver) throws IOException {
        properties=new PropertyReader().readProperty(System.getProperty("env"));
        GetUIWeatherDetailsFacade facade=new GetUIWeatherDetailsFacade(driver);
        facade.getUiWeatherDetailsFacade(properties.getProperty("cityName"));
        WeatherDetails weatherDetails=facade.getCurrentWeatherDetailsPage().getWeatherDetails();
        WeatherComparisonMetrics uiMetrics=new WeatherComparisonMetrics();
        uiMetrics.setWeatherMetrics(Double.parseDouble(weatherDetails.getTemperature().replaceAll("[^0-9]", "")),Double.parseDouble(weatherDetails.getPressure().replaceAll("[^0-9]", "")),Double.parseDouble(weatherDetails.getHumidity().replaceAll("[^0-9]", "")));
        return uiMetrics;
    }
    //Method to get weather details from API
    public WeatherComparisonMetrics getApiWeatherMetrics() throws IOException {
        GETCityWeatherClient getCityWeatherClient=new GETCityWeatherClient();
        WeatherComparisonMetrics weatherComparisonMetrics=new WeatherComparisonMetrics();
        properties=new PropertyReader().readProperty(System.getProperty("env"));
        GETCityWeatherResponse getCityWeatherResponse =getCityWeatherClient.getCityWeather();
        weatherComparisonMetrics.setWeatherMetrics(getCityWeatherResponse.getTemp(),getCityWeatherResponse.getPressure(),getCityWeatherResponse.getHumidity());
        return weatherComparisonMetrics;
    }
    //Compare Results from two sources based on given metrics
    public String compareMetric(String metricName,WebDriver driver) throws Exception {
        WeatherComparisonMetrics accuWeatherComparisonMetrics=getAccuWeatherMetrics(driver);
        WeatherComparisonMetrics apiComparisonMetrics=getApiWeatherMetrics();
        switch (metricName){
            case "temperature":
                return new MetricVarianceCalculator().getMetricVariance(apiComparisonMetrics.getTemperature(),accuWeatherComparisonMetrics.getTemperature());
            case "pressure":
                return new MetricVarianceCalculator().getMetricVariance(apiComparisonMetrics.getPressure(),accuWeatherComparisonMetrics.getPressure());
            case "humidity":
                return new MetricVarianceCalculator().getMetricVariance(apiComparisonMetrics.getHumidity(),accuWeatherComparisonMetrics.getHumidity());
            default:
                throw new Exception("Invalid Metric");
        }
    }
    //teardown method to close WebDriver
    public void tearDown(){
        DriverFactory.closeAllDriver();
    }

    //main method
    public static void main(String[] args) throws Exception {
        WeatherComparator weatherComparator=new WeatherComparator();
        weatherComparator.driverSetup();
        weatherComparator.compareMetric("temperature",driver);
        weatherComparator.tearDown();
    }
}
