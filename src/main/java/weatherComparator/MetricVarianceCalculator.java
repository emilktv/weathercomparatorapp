package weatherComparator;

import exceptions.MatcherException;
import utilities.PropertyReader;

import java.io.IOException;
import java.util.Properties;

//The methods in This class returns match if input metrics difference are within varianceRange else a Matcher Exception is thrown
public class MetricVarianceCalculator {
    public String getMetricVariance(double metric1,double metric2) throws IOException, MatcherException {
        Properties properties=new PropertyReader().readProperty(System.getProperty("env"));
        System.out.println("The difference is "+Math.abs(metric1-metric2));
        if (Math.abs(metric1-metric2)<=Double.parseDouble(properties.getProperty("varianceRange"))){
            System.out.println("Metrics match");
            return "Metrics match";
        }
        else
            System.out.println("Matcher Exception: Metrics do not match");
            throw new MatcherException("Metrics do not match");
    }
}
