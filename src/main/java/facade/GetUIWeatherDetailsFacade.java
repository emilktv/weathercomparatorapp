package facade;

import accuWeatherPages.AdFrame;
import accuWeatherPages.CurrentWeatherDetailsPage;
import accuWeatherPages.HomePage;
import accuWeatherPages.WeatherPage;
import org.openqa.selenium.WebDriver;
import utilities.AdFrameNavigator;

//This Facade instantiates Page Objects and performs web element actions.
//This Facade implements the whole procedure to search a city in accuWeather and obtain Weather Details
public class GetUIWeatherDetailsFacade {
    WebDriver driver;
    HomePage homePage;
    WeatherPage weatherPage;
    CurrentWeatherDetailsPage currentWeatherDetailsPage;
    AdFrame adFrame;
    AdFrameNavigator adFrameNavigator;

    public GetUIWeatherDetailsFacade(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        if(homePage==null){
            homePage=new HomePage(driver);
            return homePage;
        }
        else
            return homePage;
    }

    public WeatherPage getWeatherPage() {
        if(weatherPage==null){
            weatherPage=new WeatherPage(driver);
            return weatherPage;
        }
        else
            return weatherPage;
    }

    public CurrentWeatherDetailsPage getCurrentWeatherDetailsPage(){
        if(currentWeatherDetailsPage==null){
            currentWeatherDetailsPage=new CurrentWeatherDetailsPage(driver);
            return currentWeatherDetailsPage;
        }
        else
            return currentWeatherDetailsPage;
    }

    public AdFrame getAdFrame(){
        if(adFrame==null){
            adFrame=new AdFrame(driver);
            adFrameNavigator=new AdFrameNavigator(driver);
            adFrame=adFrameNavigator.switchToAdFrame();
            return adFrame;
        }
        else
            return adFrame;
    }

    public void getUiWeatherDetailsFacade(String cityName){
        getHomePage().enterCityName(cityName);
        getHomePage().clickFirstSearchResult();
        getWeatherPage().clickMoreDetailsLink();
        getAdFrame().clickDismissAdButton();
        adFrameNavigator.switchToDefaultPage();
    }
}
