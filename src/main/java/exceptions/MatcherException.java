package exceptions;

//Matcher Exception to be thrown when Metrics do not match
public class MatcherException extends Exception{
    public MatcherException(String str)
    {
        super(str);
    }
}
