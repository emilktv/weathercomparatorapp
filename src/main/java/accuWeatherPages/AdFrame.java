package accuWeatherPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utilities.PageInitializer;

public class AdFrame extends PageInitializer {
    public AdFrame(WebDriver driver){super(driver);}

    //AdFrame Elements
    @FindBy(xpath = "//*[@id=\"dismiss-button\"]")
    WebElement dismissAdButton;

    //Element Actions
    public void clickDismissAdButton(){
        dismissAdButton.click();
    }

}
