package accuWeatherPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utilities.PageInitializer;
/*
 * HomePage inherits PageInitializer. PageInitializer initializes HomePage as a PageObject.
 */
public class HomePage extends PageInitializer {
    public HomePage(WebDriver driver){super(driver);}

    // Identifying Homepage elements
    @FindBy(name = "query")
    private WebElement queryInputBox;
    
    @FindBy(xpath = "//div[@class=\"results-container\"]/div")
    private WebElement firstSearchResult;

    // WebElement Action Methods
    public void enterCityName(String cityName){
        queryInputBox.sendKeys(cityName);
        queryInputBox.click();
    }
    public void clickFirstSearchResult(){firstSearchResult.click();}
}
