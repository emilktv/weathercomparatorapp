package accuWeatherPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utilities.PageInitializer;

public class WeatherPage extends PageInitializer {
    public WeatherPage(WebDriver driver){super(driver);}
    //WeatherPage elements
    @FindBy(xpath="//div[@class=\"content-module\"]//following::a[contains(@class,'cur-con-weather-card')]")
    WebElement moreDetailsLink;

    //Element Actions
    public void clickMoreDetailsLink(){
        moreDetailsLink.click();
    }
}
