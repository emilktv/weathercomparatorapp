package accuWeatherPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utilities.PageInitializer;
import utilities.WeatherDetails;

public class CurrentWeatherDetailsPage extends PageInitializer {
    public CurrentWeatherDetailsPage(WebDriver driver){super(driver);}

    //WeatherDetailsPage elements
    @FindBy(id = "google_ads_iframe_/6581/web/in/interstitial/weather/local_home_0")
    WebElement adIframe;
    @FindBy(id="dismiss-button")
    WebElement dismissAdButton;
    @FindBy(xpath = "//div[@class=\"display-temp\"]")
    WebElement temperatureText;
    @FindBy(xpath = "//div[text()=\"Humidity\"]//following::div")
    WebElement humidityText;
    @FindBy(xpath = "//div[text()=\"Wind\"]/following-sibling::div")
    WebElement windSpeedText;
    @FindBy(xpath = "//div[text()=\"Pressure\"]/following-sibling::div")
    WebElement pressureText;
    @FindBy(xpath = "//h1[@class=\"header-loc\"]")
    WebElement cityNameHeader;

    //Element Actions
    public String getTemperatureText(){
        return temperatureText.getText();
    }
    public  String getHumidityText(){
        return humidityText.getText();
    }
    public  String getPressureText(){
        return pressureText.getText();
    }
    public String getCityNameHeaderText(){return cityNameHeader.getText();}
    public WeatherDetails getWeatherDetails(){
        WeatherDetails weatherDetails=new WeatherDetails();
        weatherDetails.setWeatherDetails(temperatureText.getText(),pressureText.getText(),humidityText.getText());
        return weatherDetails;
    }

}
