package utilities;

public class WeatherDetails {
    String temperature;
    String pressure;
    String humidity;

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getPressure() {
        return pressure;
    }

    public void setPressure(String pressure) {
        this.pressure = pressure;
    }

    public void setWeatherDetails(String temperature,String pressure,String humidity){
        this.temperature=temperature;
        this.pressure=pressure;
        this.humidity=humidity;
    }
}
