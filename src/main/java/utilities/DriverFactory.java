package utilities;

import eventCapture.EventCapture;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.HashMap;
import java.util.Map;
/*
 * The DriverFactory Class instantiates and maintains WebDriver instances
 */
public class DriverFactory {
    // HashMap to store/cache browser drivers in session. Private so that it cannot be accessed
    private static Map<String,WebDriver> drivers=new HashMap<String,WebDriver>();
    public static EventFiringWebDriver eventFiringWebDriver;
    public static EventCapture eventCapture;
    // getDriver() is a browser factory method which returns WebDriver instance
    public static WebDriver getDriver(String browserName){
        //driver holds WebDriver instance
        WebDriver driver=null;
        /*
         * switch case to assign WebDriver instance to driver
         * Assign value (WebDriver) of key (browserName) from hashmap to driver
         * driver is null if key value pair does not exist in hashmap
         * If driver is null, create new browser driver and put to HashMap
         * WebDriverManager is used to instantiate WebDriver instatnces
         */
        switch(browserName){
            case "Chrome":
                driver=drivers.get("Chrome");
                if (driver==null)
                {
                    //Headless mode test failure screenshots showed Access Denied to server
                    WebDriverManager.chromedriver().setup();
                    driver=new ChromeDriver();
                    drivers.put("Chrome",driver);
                }
                break;
            case "Firefox":
                driver=drivers.get("Firefox");
                if (driver==null)
                {
                    WebDriverManager.firefoxdriver().setup();
                    driver=new FirefoxDriver();
                    drivers.put("FireFox",driver);
                }
                break;
            case "Edge":
                driver=drivers.get("Edge");
                if (driver==null)
                {
                    WebDriverManager.edgedriver().setup();
                    driver = new EdgeDriver();
                    drivers.put("Edge",driver);
                }
                break;
        }
        //return WebDriver instance
        //return driver;
        eventFiringWebDriver=new EventFiringWebDriver(driver);
        eventCapture=new EventCapture();
        eventFiringWebDriver.register(eventCapture);
        return eventFiringWebDriver;
    }
    public static void closeAllDriver() {
        for (String key : drivers.keySet()) {
            drivers.get(key).close();
            drivers.get(key).quit();
        }

    }
}
