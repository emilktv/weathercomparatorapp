package utilities;

import java.io.FileReader;
import java.io.IOException;
import java.util.Properties;

public class PropertyReader {
    public Properties readProperty(String env) throws IOException {
        FileReader reader=new FileReader(System.getProperty("user.dir")+"/src/test/resources/environments/"+String.format("%s",env)+"env.properties");
        Properties properties=new Properties();
        properties.load(reader);
        return properties;
    }

}
