package utilities;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Date;

public class CaptureScreenshot {
    public void capture(WebDriver driver){
        TakesScreenshot scrShot =((TakesScreenshot)driver);
        File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);
        String filename =  new SimpleDateFormat("yyyyMMddhhmmss'.jpeg'").format(new Date());
        File DestFile=new File("./src/test/TestScreenshots/"+filename);
        try{
            FileUtils.copyFile(SrcFile, DestFile);}
        catch (Exception e){
            System.out.println(e.toString());
        }
        System.out.println("Screenshot Captured");
    }
}
