package utilities;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/*
 * PageInitializer initializes WebElements defined in PageObjects
 */
public class PageInitializer {
    /*
     * PageFactory.initElements() takes the WebDriver instance of the given class and class type.
     * initElements() returns a Page Object with initialized components
     */
    public PageInitializer(WebDriver driver){ PageFactory.initElements(driver,this); }
}
