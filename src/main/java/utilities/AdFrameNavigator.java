package utilities;

import accuWeatherPages.AdFrame;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//AdFrame Navigator to switch to and return AdFrame pageObject
public class AdFrameNavigator {
    WebDriver driver;
    public AdFrameNavigator(WebDriver driver){
        this.driver=driver;
    }
    //return AdFrame Page Object after switching to AdFrame
    public AdFrame switchToAdFrame(){
        driver.switchTo().frame(driver.findElement(By.id("google_ads_iframe_/6581/web/in/interstitial/weather/local_home_0")));
        return new AdFrame(driver);
    }
    //Switch back to Default Page
    public void switchToDefaultPage(){
        driver.switchTo().defaultContent();
    }
}
